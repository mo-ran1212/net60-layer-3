﻿using AspNetCoreRateLimit;
using EntityFrameworkCore.MigrationTool;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;
namespace EntityFrameworkCore.Supreme.Extension
{
    /// <summary>
    /// 描述：自定义ServiceExtensions
    /// 作者：杨钊
    /// 时间：2023-2-24
    /// </summary>
    public static class CustomizeServiceExtensions
    {
        private static readonly IConfiguration configuration = AppHelper.GetConfiguration();

        /// <summary>
        /// 描述：Authentication的JwtBearer验证服务注册
        /// 作者：杨钊
        /// 时间：2023-2-24
        /// </summary>
        /// <param name="services"></param>
        public static void AuthenticationConfigure(this IServiceCollection services)
        {
            // 注册服务
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true, //是否验证Issuer
                    ValidIssuer = configuration["Jwt:Issuer"], //发行人Issuer
                    ValidateAudience = true, //是否验证Audience
                    ValidAudience = configuration["Jwt:Audience"], //订阅人Audience
                    ValidateIssuerSigningKey = true, //是否验证SecurityKey
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:SecretKey"])), //SecurityKey
                    ValidateLifetime = true, //是否验证失效时间
                    ClockSkew = TimeSpan.FromSeconds(30), //过期时间容错值，解决服务器端时间不同步问题（秒）
                    RequireExpirationTime = true,
                };
            });
        }
        /// <summary>
        /// 描述：Api限流配置
        /// 作者：杨钊
        /// 时间：2023-2-23
        /// </summary>
        /// <param name="services"></param>
        public static void ApiVersioningConfigure(this IServiceCollection services)
        {
            var configuration = AppHelper.GetConfiguration();
            services.AddOptions();
            services.AddMemoryCache();
            services.Configure<IpRateLimitOptions>(configuration.GetSection("IpRateLimiting"));
            services.Configure<IpRateLimitPolicies>(configuration.GetSection("IpRateLimitPolicies"));
            services.AddHttpContextAccessor();
            services.AddInMemoryRateLimiting();
            services.AddMvc();
            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
            services.AddSingleton<IIpPolicyStore, DistributedCacheIpPolicyStore>();
            services.AddSingleton<IRateLimitCounterStore, DistributedCacheRateLimitCounterStore>();
        }

        /// <summary>
        /// 描述：Swagger配置
        /// 作者：杨钊
        /// 时间：2023-2-23
        /// </summary>
        /// <param name="services"></param>
        public static void SwaggerGenConfigure(this IServiceCollection services)
        {
            services.AddSwaggerGen(x =>
             {
                 x.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                 {
                     Description = "在下框中输入请求头中需要添加Jwt授权Token：Bearer Token",
                     Name = "Authorization",
                     In = ParameterLocation.Header,
                     Type = SecuritySchemeType.ApiKey,
                     BearerFormat = "JWT",
                     Scheme = "Bearer"
                 });  //全局配置
                 x.AddSecurityRequirement(new OpenApiSecurityRequirement
                 {
                     {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },Array.Empty<string>()
                     }
                 });//配置接口加入Bearer Token
                 x.SwaggerDoc("v1", new OpenApiInfo
                 {
                     Title = "Zenith.NetCore",
                     Version = "v1",
                     Description = "Zenith.NetCore接口文档"
                 });
                 var file = Path.Combine(AppContext.BaseDirectory, "Zenith.NetCore.xml");  // xml文档绝对路径
                 var path = Path.Combine(AppContext.BaseDirectory, file); // xml文档绝对路径
                 x.IncludeXmlComments(path, true); // true : 显示控制器层注释
                 x.OrderActionsBy(o => o.RelativePath); // 对action的名称进行排序，如果有多个，就可以看见效果了。
             });//Swagger接口
        }

    }
}
