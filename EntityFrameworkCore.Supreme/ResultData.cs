﻿using EntityFrameworkCore.MigrationTool;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkCore.Supreme
{
    /// <summary>
    /// 返回结果
    /// </summary>
    public static class ResultData
    {
        /// <summary>
        /// 返回成功状态
        /// </summary>
        /// <param name="data"></param>
        /// <param name="msg"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static ResultJson SuccessBool(string msg = "请求成功！", int code = 200)
        {
            return new ResultJson
            {
                Msg = msg,
                Code = code
            };
        }
        /// <summary>
        /// 返回成功状态
        /// </summary>
        /// <param name="data"></param>
        /// <param name="msg"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static ResultJson<T> Success<T>(this T data, string msg = "请求成功！", int code = 200)
        {
            return new ResultJson<T>
            {
                Msg = msg,
                Code = code,
                Data = data
            };
        }
        /// <summary>
        /// 返回失败状态
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static ResultJson Fail(string msg = "请求失败！", string className = "All", int code = 400)
        {
            Log.Warn(msg, className);
            return new ResultJson
            {
                Msg = msg,
                Code = code
            };
        }
        /// <summary>
        /// 请求返回成功
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static ResultPageJson<T> SuccessList<T>(this List<T> list, int count)
        {
            return new ResultPageJson<T>
            {
                Code = 0,
                Msg = "请求成功！",
                Count = count,
                Data = list
            };
        }
    }
    public class ResultJson
    {
        public int Code { get; set; }
        public string Msg { get; set; }
    }
    public class ResultJson<T>
    {
        public int Code { get; set; }
        public string Msg { get; set; }

        public T Data { get; set; }
    }
    public class ResultPageJson<T>
    {
        public int Code { get; set; }
        public string Msg { get; set; }

        public int Count { get; set; }

        public List<T> Data { get; set; }
    }
}
