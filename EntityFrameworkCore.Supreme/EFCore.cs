﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Data.Common;
using System.Data;
using System.Linq.Expressions;
using System.Reflection;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore.Internal;

namespace EntityFrameworkCore.Supreme
{
    public static class EFCore
    {
        /// <summary>
        /// 反射实现两个类的复制
        /// </summary>
        /// <typeparam name="D">主</typeparam>
        /// <typeparam name="S">副</typeparam>
        /// <param name="d">主</param>
        /// <param name="s">副</param>
        /// <param name="bol">是否去空复制，默认flase</param>
        /// <returns>主实体</returns>
        public static D Copy<D, S>(this D d, S s, bool bol = false)
        {
            try
            {
                var Types = typeof(S);
                var Typed = typeof(D);
                foreach (PropertyInfo sp in Types.GetProperties())//获得类型的属性字段  
                {
                    foreach (PropertyInfo dp in Typed.GetProperties())
                    {
                        if (dp.Name == sp.Name && dp.PropertyType.FullName.Contains(sp.PropertyType.FullName ?? "") && dp.Name != "Error" && dp.Name != "Item")//判断属性名是否相同  
                        {
                            if (!bol)
                            {
                                dp.SetValue(d, sp.GetValue(s, null), null);//获得s对象属性的值复制给d对象的属性  
                            }
                            else
                            {
                                if (sp.GetValue(s, null) != null)
                                {
                                    dp.SetValue(d, sp.GetValue(s, null), null);//获得s对象属性的值复制给d对象的属性  
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return d;
        }
        /// <summary>
        /// 条件IF
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IQueryable<TSource> WhereIF<TSource>(this IQueryable<TSource> source, bool fs, Expression<Func<TSource, bool>> predicate)
        {
            if (fs)
            {
                return source.Where(predicate);
            }
            return source;
        }
        /// <summary>
        /// 返回sql
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="dataQueryable"></param>
        /// <returns></returns>
        public static string ToSql<TSource>(this IQueryable<TSource> dataQueryable)
        {
            var sql = ((Microsoft.EntityFrameworkCore.Query.Internal.EntityQueryable<TSource>)dataQueryable).DebugView.Query;
            return sql;
        }

        /// <summary>
        /// 分页List同步
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static List<TSource> ToPageList<TSource>(this IQueryable<TSource> source, int page, int pageSize)
        {
            return source.Skip((page - 1) * pageSize).Take(pageSize).ToList();
        }

        /// <summary>
        /// 分页List异步
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static async Task<List<TSource>> ToPageListAsync<TSource>(this IQueryable<TSource> source, int page, int pageSize)
        {
            return await source.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
        }
        /// <summary>
        /// Sql查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="facade">Database</param>
        /// <param name="sql">sql语句</param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static IEnumerable<T> SqlQuery<T>(this DatabaseFacade facade, string sql, params object[] parameters) where T : class, new()
        {
            DataTable dt = SqlQuery(facade, sql, parameters);
            return dt.ToEnumerable<T>();
        }

        private static IEnumerable<T> ToEnumerable<T>(this DataTable dt) where T : class, new()
        {
            PropertyInfo[] propertyInfos = typeof(T).GetProperties();
            T[] ts = new T[dt.Rows.Count];
            int i = 0;
            foreach (DataRow row in dt.Rows)
            {
                T t = new T();
                foreach (PropertyInfo p in propertyInfos)
                {
                    if (dt.Columns.IndexOf(p.Name) != -1 && row[p.Name] != DBNull.Value)
                        p.SetValue(t, row[p.Name], null);
                }
                ts[i] = t;
                i++;
            }
            return ts;
        }

        public static DataTable SqlQuery(this DatabaseFacade facade, string sql, params object[] parameters)
        {
            DbCommand cmd = CreateCommand(facade, sql, out DbConnection conn, parameters);
            DbDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            reader.Close();
            conn.Close();
            return dt;
        }

        private static DbCommand CreateCommand(DatabaseFacade facade, string sql, out DbConnection dbConn, params object[] parameters)
        {
            DbConnection conn = facade.GetDbConnection();
            dbConn = conn;
            conn.Open();
            DbCommand cmd = conn.CreateCommand();
            cmd.CommandText = sql;
            if (!facade.IsMySql())
            {
                CombineParams(ref cmd, parameters);

            }
            return cmd;
        }

        private static void CombineParams(ref DbCommand command, params object[] parameters)
        {
            if (parameters != null)
            {
                foreach (SqlParameter parameter in parameters.Cast<SqlParameter>())
                {
                    if (!parameter.ParameterName.Contains('@'))
                        parameter.ParameterName = $"@{parameter.ParameterName}";
                    command.Parameters.Add(parameter);
                }
            }
        }

    }
}
