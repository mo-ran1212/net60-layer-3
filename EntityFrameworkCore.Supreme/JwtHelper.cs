﻿using EntityFrameworkCore.MigrationTool;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Runtime.ConstrainedExecution;
using System.Security.Claims;
using System.Text;
namespace EntityFrameworkCore.Supreme;
public static class JwtHelper
{
    /// <summary>
    /// 生成Token
    /// </summary>
    /// <param name="authClaims"></param>
    /// <returns></returns>
    public static string CreateJwtToken(List<Claim> authClaims)
    {
        var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppHelper.ReadAppSettings("Jwt", "SecretKey")));
        var algorithm = SecurityAlgorithms.HmacSha256;
        var signingCredentials = new SigningCredentials(secretKey, algorithm);
        var jwtSecurityToken = new JwtSecurityToken(
            AppHelper.ReadAppSettings("Jwt", "Issuer"),
            AppHelper.ReadAppSettings("Jwt", "Audience"),
            authClaims,
            DateTime.Now,
            DateTime.Now.AddHours(int.Parse(AppHelper.ReadAppSettings("Jwt", "TimeHours"))),
            signingCredentials
        );
        var token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
        return token;
    }
    /// <summary>
    /// 校验token
    /// </summary>
    /// <param name="httpContextAccessor">httpContext</param>
    /// <returns></returns>
    public static (bool, UserInfo) VerifyJwtToken(this IHttpContextAccessor httpContextAccessor)
    {
        var token = httpContextAccessor.HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer", "").Replace(" ", "");
        return VerifyJwtToken(token);
    }
    /// <summary>
    /// 校验token
    /// </summary>
    /// <param name="httpContextAccessor">httpContext</param>
    /// <returns></returns>
    public static UserInfo GetUserInfo(this IHttpContextAccessor httpContextAccessor)
    {
        var token = httpContextAccessor.HttpContext.User.Claims;

        return new UserInfo
        {
            //ID=token.Where(x=>x.Value=="ID"),
            //UserName=token.UserName,

        };
    }
    /// <summary>
    /// 校验token
    /// </summary>
    /// <param name="httpContextAccessor">httpContext</param>
    /// <returns></returns>
    public static (bool, UserInfo) VerifyIdentity(this IEnumerable<Claim> claims)
    {
        //claims.Where(x=>x.Value=="ID")
        return (true, new UserInfo());
    }
    /// <summary>
    /// 校验token
    /// </summary>
    /// <param name="token">token串</param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    public static (bool, UserInfo) VerifyJwtToken(this string token)
    {
        //对称秘钥
        SecurityKey securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(AppHelper.ReadAppSettings("Jwt", "SecretKey")));
        //校验token
        var validateParameter = new TokenValidationParameters()
        {
            ValidateLifetime = true,
            ValidateAudience = true,
            ValidateIssuer = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = AppHelper.ReadAppSettings("Jwt", "Issuer"),
            ValidAudience = AppHelper.ReadAppSettings("Jwt", "Audience"),
            IssuerSigningKey = securityKey,
            ClockSkew = TimeSpan.Zero//校验过期时间必须加此属性
        };
        //不校验，直接解析token
        //jwtToken = new JwtSecurityTokenHandler().ReadJwtToken(token1);
        UserInfo userInfo;
        bool success;
        try
        {
            //校验并解析token,validatedToken是解密后的对象
            ClaimsPrincipal principal = new JwtSecurityTokenHandler().ValidateToken(token, validateParameter, out SecurityToken validatedToken);
            //获取payload中的数据 
            var jwtPayload = ((JwtSecurityToken)validatedToken).Payload.SerializeToJson();
            userInfo = JsonConvert.DeserializeObject<UserInfo>(jwtPayload);
            if (userInfo != null) success = true;
            success = true;
        }
        catch (SecurityTokenExpiredException)
        {
            throw new NotImplementedException("登录失效，请重新登录！");
        }
        catch (SecurityTokenException)
        {
            throw new NotImplementedException("身份验证失败，请重新登录！");
        }
        catch (Exception ex)
        {
            throw new NotImplementedException(ex.Message);
        }
        return (success, userInfo);
    }
}