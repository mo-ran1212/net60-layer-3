﻿namespace EntityFrameworkCore.Supreme
{
    public class UserInfo
    {
        /// <summary>
        /// ID
        /// </summary>
        public long ID { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 角色
        /// </summary>
        public string RoleIds { get; set; }
    }
}
