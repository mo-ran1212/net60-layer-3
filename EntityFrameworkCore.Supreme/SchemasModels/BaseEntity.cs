﻿using System;
using System.Collections.Generic;
using System.Reflection;
namespace EntityFrameworkCore.Supreme.SchemasModels
{
    /// <summary>
    /// 数据库实体类
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// 数据库转.Net字段类型
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string ChangeToCSharpType(string type)
        {
            string reval = string.Empty;
            switch (type.ToLower())
            {
                case "int":
                    reval = "int";
                    break;
                case "text":
                    reval = "string";
                    break;
                case "bigint":
                    reval = "long";
                    break;
                case "binary":
                    reval = "byte[]";
                    break;
                case "bit":
                    reval = "bool";
                    break;
                case "char":
                    reval = "string";
                    break;
                case "datetime":
                    reval = "DateTime";
                    break;
                case "decimal":
                    reval = "decimal";
                    break;
                case "float":
                    reval = "double";
                    break;
                case "image":
                    reval = "byte[]";
                    break;
                case "money":
                    reval = "decimal";
                    break;
                case "nchar":
                    reval = "string";
                    break;
                case "ntext":
                    reval = "string";
                    break;
                case "numeric":
                    reval = "system.Decimal";
                    break;
                case "nvarchar":
                    reval = "string";
                    break;
                case "real":
                    reval = "system.Single";
                    break;
                case "smalldatetime":
                    reval = "DateTime";
                    break;
                case "smallint":
                    reval = "Int16";
                    break;
                case "smallmoney":
                    reval = "decimal";
                    break;
                case "timestamp":
                    reval = "DateTime";
                    break;
                case "tinyint":
                    reval = "byte";
                    break;
                case "uniqueidentifier":
                    reval = "Guid";
                    break;
                case "varbinary":
                    reval = "byte[]";
                    break;
                case "varchar":
                    reval = "string";
                    break;
                case "Variant":
                    reval = "object";
                    break;
                default:
                    reval = "string";
                    break;
            }
            return reval;
        }

        /// <summary>
        /// 主键名称
        /// </summary>
        public string PrimaryKey { get; set; }

        /// <summary>
        /// 数据库名
        /// </summary>
        public string SqlName { get; set; }
        /// <summary>
        /// 实体名
        /// </summary>
        public string MoldesName { get; set; }
        /// <summary>
        /// Ip地址
        /// </summary>
        public string Ip { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 用户密码
        /// </summary>
        public string UserPw { get; set; }
        /// <summary>
        /// 继承公共类
        /// </summary>
        public string PublicClass { get; set; }
        /// <summary>
        /// 生成地址
        /// </summary>
        public string Paths { get; set; }
    }
    /// <summary>
    /// 数据库实体类
    /// </summary>
    public class BaseMysqlEntity
    {

        /// <summary>
        /// MySql数据类型简称对应.NET类型
        /// MySql数据类型(MySqlDbType)名称对应.NET类型(Type)
        /// </summary>
        public static IDictionary<string, string> MySqlDbTypeName2NetTypeMapping
        {
            get
            {
                IDictionary<string, string> map = new Dictionary<string, string>();
                map.Add("BigInt", "long");
                map.Add("Binary", "byte[]");
                map.Add("Bit", "bool");
                map.Add("Blob", "byte[]");
                map.Add("Char", "char");
                map.Add("Byte", "byte");
                map.Add("Date", "DateTime");
                map.Add("DateTime", "DateTime");
                map.Add("Decimal", "decimal");
                map.Add("Double", "double");
                map.Add("Enum", "string");//enum可以自定义类型，常用的是0，1，也可以定义为boy,girl,secret
                map.Add("Float", "double");
                map.Add("Geometry", "object");
                map.Add("Guid", "Guid");
                map.Add("Int", "int");
                map.Add("Integer", "int");
                map.Add("Int16", "short");
                map.Add("Int24", "int");
                map.Add("Int32", "int");
                map.Add("Int64", "long");
                map.Add("JSON", "string");
                map.Add("LongBlob", "byte[]");
                map.Add("LongText", "string");
                map.Add("MediumBlob", "byte[]");
                map.Add("MediumText", "string");
                map.Add("Newdate", "DateTime");
                map.Add("NewDecimal", "decimal");
                map.Add("Numeric", "decimal");
                map.Add("Real", "double");
                map.Add("Set", "string");
                map.Add("String", "string");
                map.Add("SmallInt", "short");
                map.Add("Text", "string");
                map.Add("Time", "DateTime");
                map.Add("Timestamp", "DateTime");
                map.Add("TinyBlob", "byte[]");
                map.Add("TinyInt", "short");
                map.Add("TinyText", "string");
                map.Add("UByte", "byte");
                map.Add("UInt16", "ushort");
                map.Add("UInt24", "uint");
                map.Add("UInt32", "uint");
                map.Add("UInt64", "ulong");
                map.Add("VarBinary", "byte[]");
                map.Add("VarChar", "string");
                map.Add("VarString", "string");
                map.Add("Year", "int");
                return map;
            }
        }

        /// <summary>
        /// 主键名称
        /// </summary>
        public string PrimaryKey { get; set; }
        /// <summary>
        /// 数据库名
        /// </summary>
        public string SqlName { get; set; }
        /// <summary>
        /// 实体名
        /// </summary>
        public string MoldesName { get; set; }
        /// <summary>
        /// 继承公共类
        /// </summary>
        public string PublicClass { get; set; }
        /// <summary>
        /// Context生成地址
        /// </summary>
        public string ContextPaths { get; set; }

        /// <summary>
        /// 生成地址
        /// </summary>
        public string Paths { get; set; }
        /// <summary>
        /// 命名空间
        /// </summary>
        public string Namespace { get; set; }

        /// <summary>
        /// Context命名空间
        /// </summary>
        public string ContextNamespace { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>

        public string ProjrctName{ get; set; }
}
    /// <summary>
    /// 表类
    /// </summary>
    public class Table
    {
        /// <summary>
        /// 
        /// </summary>
        public string RowNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TableSm { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FieldNum { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FieldName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Identify { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PrimaryKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Bytes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Lengh { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DecimalPlaces { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AllowNull { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Default { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FieldFescription { get; set; }
    }
    /// <summary>
    /// Mysql表类
    /// </summary>
    public class MysqlTable
    {
        /// <summary>
        /// 
        /// </summary>
        public string ColumnName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DataType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ColumnComment { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ColumnKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Extra { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IsNullable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ColumnType { get; set; }

    }

    /// <summary>
    /// Mysql表Name
    /// </summary>
    public class TableMysqlName
    {
        /// <summary>
        /// 表名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 表类型
        /// </summary>
        public string TypeEngine { get; set; }
        /// <summary>
        /// 表类型2
        /// </summary>
        public string TypeTableComment { get; set; }
    }

    /// <summary>
    /// 表Name
    /// </summary>
    public class TableName
    {
        /// <summary>
        /// 表名
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 表类型
        /// </summary>
        public string type { get; set; }
    }
    /// <summary>
    /// 表Models
    /// </summary>
    public class TableModels
    {
        /// <summary>
        /// 表名
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 表str
        /// </summary>
        public string str { get; set; }
        /// <summary>
        /// 表类型
        /// </summary>
        public string type { get; set; }
    }
}
