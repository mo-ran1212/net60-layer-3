﻿using EntityFrameworkCore.Supreme.SchemasModels;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
namespace EntityFrameworkCore.Supreme
{
    public static class DataBaseEntity
    {
        /// <summary>
        /// 创建EFCore实体模型
        /// </summary>
        /// <param name="entity">实体参数配置</param>
        /// <param name="fields">过滤字段</param>
        public static void CreateMySqlEntity(this DbContext Db, BaseMysqlEntity entity, List<string> fields)
        {
            var type = "UseMySql";
            if (!string.IsNullOrWhiteSpace(entity.PublicClass))
            {
                entity.PublicClass = ": " + entity.PublicClass;
            }
            var DbConnection = Db.Database.GetDbConnection();
            DbConnection.Open();
            var version = DbConnection.ServerVersion;
            var connectionString = DbConnection.ConnectionString;
            DbConnection.Close();
            List<TableModels> liststr = new List<TableModels>();
            string Context = @"using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using " + entity.Namespace + @";
namespace " + entity.ContextNamespace + @"
{
    public partial class " + entity.MoldesName + @" : DbContext
    {
        public " + entity.MoldesName + @"()
        {
        }
        private readonly IHttpContextAccessor _httpContextAccessor;
        public " + entity.MoldesName + @"(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public " + entity.MoldesName + @"(DbContextOptions<" + entity.MoldesName + @"> options) : base(options) { }";
            var listName = Db.Database.SqlQuery<TableMysqlName>(@"select table_name Name,  engine AS TypeEngine,  table_comment TypeTableComment from information_schema.tables where table_schema = (select database()) order by create_time desc");
            listName.ToList().ForEach(x =>
            {
                Context += @"
        public virtual DbSet<" + x.Name + "> " + x.Name + " { get; set; }";
                var sql = @"select column_name as ColumnName,data_type as DataType,column_comment as ColumnComment,column_key as ColumnKey,extra as Extra,is_nullable as IsNullable,column_type as ColumnType 
                        from information_schema.columns where table_name = '" + x.Name + @"' and table_schema = (select database()) order by ordinal_position;";
                var list = Db.Database.SqlQuery<MysqlTable>(sql);
                string txt = @"using System;
using System.Collections.Generic;

namespace " + entity.Namespace + @"
{" + @"
    public partial class " + x.Name + @" " + entity.PublicClass + @"
    {";
                list.ToList().ForEach(t =>
                {
                    bool fa = true;
                    if (fields != null && fields.Count > 0)
                    {
                        for (int i = 0; i < fields.Count; i++)
                        {
                            if (fields.Contains(t.ColumnName))
                            {
                                fa = false;
                            }
                        }
                    }
                    if (fa)
                    {
                        var type = BaseMysqlEntity.MySqlDbTypeName2NetTypeMapping.Where(x => x.Key.ToLower() == t.DataType).FirstOrDefault().Value;
                        if (t.IsNullable == "YES" && type != "string")
                        {
                            type += "?";
                        }
                        if (!string.IsNullOrWhiteSpace(t.ColumnComment))
                        {
                            txt += @" 
        /// <summary>
        /// " + t.ColumnComment + @"
        /// </summary>";
                        }
                        txt += @"
        public " + type + " " + t.ColumnName + " { get; set; }";
                    }
                });
                txt += @"
    }
}";
                liststr.Add(new TableModels { name = x.Name, str = txt });
            });
            Context += @"
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder." + type + "(AppHelper.ReadAppSettings(" + "\"" + entity.ProjrctName + "\", " + "\"ConnectionString\"" + ") ,ServerVersion.Parse(" + "\"" + version + "-mysql\"" + @"));
            }
            //默认禁用实体跟踪
            optionsBuilder = optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            //开启EF日志处理 所有 EF Core 消息
            optionsBuilder.LogTo(message => Log.InfoEFCore(_httpContextAccessor, message), LogLevel.Information);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {";
            listName.ToList().ForEach(t =>
            {
                if (t.TypeEngine == "InnoDB" && string.IsNullOrWhiteSpace(t.TypeTableComment))
                {
                    Context += @"
            modelBuilder.Entity<" + t.Name + @">(entity =>
            {
                entity.HasKey(e => e." + entity.PrimaryKey + @");
            });
";
                }
                else
                {
                    Context += @"
            modelBuilder.Entity<" + t.Name + @">(entity =>
            {
                entity.HasNoKey();
            });
";
                }

            });
            Context += @"
        }
    }
}";
            if (!Directory.Exists(entity.Paths))//判断文件夹是否存在
            {
                Directory.CreateDirectory(entity.Paths);//不存在则创建文件夹
            }
            File.WriteAllText(entity.ContextPaths + @"\" + entity.MoldesName + @".cs", Context);
            liststr.ForEach(t =>
            {
                File.WriteAllText(entity.Paths + @"\" + t.name + ".cs", t.str);
            });
            return;
        }
        
    }
}
