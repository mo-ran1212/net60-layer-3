﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zenith.Dao.Interfaces.Req.Account
{
    public class SignInReq
    {
        /// <summary>
        /// 账号
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        ///密码
        /// </summary>
        public string UserPwd { get; set; }
    }
}
