﻿using Microsoft.AspNetCore.Mvc;
using Service.Interface;
using Zenith.Dao.Interfaces.Req.Account;

namespace Controllers
{
    /// <summary>
    /// 账号控制器
    /// </summary>
    [ApiController]
    [Route("Api/[Controller]/[action]")]
    public class AccountController : Controller
    {

        private readonly IAccount _Account;
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="account"></param>
        public AccountController(IAccount account)
        {
            _Account = account;
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="signIn"></param>
        /// <returns></returns>
        [HttpPost(Name = "SignIn")]
        public async Task<object> SignIn([FromBody] SignInReq signIn)
        {
            var data = await _Account.SignInAsync(signIn);
            return Json(data);
        }
    }
}