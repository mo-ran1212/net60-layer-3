using Microsoft.OpenApi.Models;
using System.Reflection;
using Autofac.Extensions.DependencyInjection;
using Autofac;
using EntityFrameworkCore.Mysql.Extensions;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using log4net.Repository;
using log4net;
using EntityFrameworkCore.Mysql.RepositoryBase;
using AspNetCoreRateLimit;
using EntityFrameworkCore.MigrationTool;
using EntityFrameworkCore.Supreme.Extension;
using Zenith.Dao;
using Zenith.Service;
using Zenith.NetCore.Filter;

IConfiguration configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
ILoggerRepository repository = LogManager.CreateRepository("LogRepository");
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddSingleton(new AppHelper(configuration));
builder.Services.AddControllers();
builder.Services.AddControllers(o => o.Filters.Add(typeof(ExceptionFilter)));
builder.Services.AddControllers(o => o.Filters.Add(typeof(Operation)));
builder.Services.AddEndpointsApiExplorer();

// 注册服务 Api限流配置
builder.Services.ApiVersioningConfigure();
// 注册服务jwt 
builder.Services.AuthenticationConfigure();
// 注册服务 Swagger接口
builder.Services.SwaggerGenConfigure();

//builder.Services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);//取消小驼峰命名

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "MyNetCore", builde =>
    {
        builde.WithOrigins("*", "*", "*")
        .AllowAnyOrigin()
        .AllowAnyHeader()
        .AllowAnyMethod();
    });
});//配置跨域
builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
builder.Services.Configure<FormOptions>(options =>
{
    options.KeyLengthLimit = int.MaxValue;
    options.ValueLengthLimit = int.MaxValue;
    options.MultipartBodyLengthLimit = 1024 * 1024 * 1024;
    options.MultipartHeadersLengthLimit = 1024 * 1024 * 1024;
});//配置上传文件大小上传方式1
builder.Services.Configure<KestrelServerOptions>(options =>
{
    options.Limits.MaxRequestBodySize = 1024 * 1024 * 1024;
    options.Limits.MaxRequestBufferSize = 1024 * 1024 * 1024;
});//配置上传文件大小上传方式2
builder.Host.ConfigureContainer<ContainerBuilder>(container =>
{
    Assembly assembly = Assembly.Load(ServiceAutofac.GetAssemblyName());
    container.RegisterAssemblyTypes(assembly)
    .AsImplementedInterfaces()
    .InstancePerDependency();
});//Service批量依赖注入
builder.Host.ConfigureContainer<ContainerBuilder>(container =>
{
    Assembly assembly = Assembly.Load(DaoServiceAutofac.GetAssemblyName());
    container.RegisterAssemblyTypes(assembly)
    .AsImplementedInterfaces()
    .InstancePerDependency();
});//Dao批量依赖注入
builder.Host.ConfigureContainer<ContainerBuilder>(container => { container.RegisterType<ZenithContext>().As<ZenithContext>().InstancePerLifetimeScope(); });//单个依赖注入
builder.Host.ConfigureContainer<ContainerBuilder>(container => { container.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>(); });//单个依赖注入
builder.Services.AddScoped(typeof(IRepository<>), typeof(RepositoryBase<>));
var app = builder.Build();
if (app.Environment.IsDevelopment()) //配置是否只有开发环境显示swagger
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(x => x.SwaggerEndpoint("/swagger/v1/swagger.json", "Zenith.NetCore"));
}

app.UseHttpsRedirection();

app.UseIpRateLimiting();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.UseCors("MyNetCore");

app.Run();

