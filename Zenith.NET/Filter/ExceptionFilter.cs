﻿using EntityFrameworkCore.MigrationTool;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System.Net;

namespace Zenith.NetCore.Filter
{
    /// <summary>
    /// 异常过滤器
    /// </summary>
    public class ExceptionFilter : IAsyncExceptionFilter
    {
        /// <summary>
        /// 异常后操作
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task OnExceptionAsync(ExceptionContext context)
        {
            var t = context.ActionDescriptor as ControllerActionDescriptor;
            //t.ControllerName+" "+t.ActionName, 
            Log.Error(context.Exception.Message);
            // 定义返回类型
            var result = new
            {
                code = 500,
                message = context.Exception.Message,
            };
            context.Result = new ContentResult
            {
                // 返回状态码设置为200，表示成功
                StatusCode = (int)HttpStatusCode.OK,
                // 设置返回格式
                ContentType = "application/json;charset=utf-8",
                Content = JsonConvert.SerializeObject(result)
            };
            // 设置为true，表示异常已经被处理了
            context.ExceptionHandled = true;
            return Task.CompletedTask;
        }
    }
}
