﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkCore.MigrationTool
{
    /// <summary>
    /// 时间操作类
    /// </summary>
    public static class TimeOperation
    {
        /// <summary>
        /// 获取当前时间戳(秒)
        /// </summary>
        /// <returns></returns>
        public static long GetStampToS(this DateTime Time)
        {
            return Convert.ToInt64(Time.GetTime().TotalSeconds);
        }
        /// <summary>
        /// 获取当前时间戳(毫秒)
        /// </summary>
        /// <returns></returns>
        public static long GetStampToM(this DateTime Time)
        {
            return Convert.ToInt64(Time.GetTime().TotalMilliseconds);
        }
        /// <summary>
        /// 获取当前时间戳(毫秒)
        /// </summary>
        /// <returns></returns>
        public static DateTime GetMToStamp(this long TimeStamp)
        {
            DateTime ts = new (1970, 1, 1, 0, 0, 0, 0);
            long tt = ts.Ticks + TimeStamp * 10000;
            return new DateTime(tt);
        }
        /// <summary>
        /// 获取时间间隔（获取时间戳）
        /// </summary>
        /// <returns></returns>
        private static TimeSpan GetTime(this DateTime Time)
        {
            TimeSpan ts = Time - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return ts;
        }
        /// <summary>
        /// 判断时间是否为当天
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static bool SameDay(this DateTime dateTime)
        {
            var DateTimeS = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"));
            var DateTimeE = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 23:59:59"));
            if (dateTime < DateTimeS || dateTime > DateTimeE)
            {
                return false;
            }
            return true;
        }

    }
}
