﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkCore.MigrationTool
{
    /// <summary>
    /// 加密
    /// </summary>
    public static class Encryption
    {
        /// <summary>
        /// DES加密
        /// </summary>
        /// <param name="datastr">需要解密的字符串</param>
        /// <param name="KEY_16">加密秘钥</param>
        /// <param name="IV_8">偏移量</param>
        /// <returns>返回加密后的字符串</returns>
        public static string SetCode(object datastr, string KEY_16, string IV_8)
        {
            try
            {
                byte[] byKey = Encoding.ASCII.GetBytes(KEY_16);
                byte[] byIV = Encoding.ASCII.GetBytes(IV_8);
                var desc = TripleDES.Create();
                var strJson = JsonConvert.SerializeObject(datastr);
                byte[] data = Encoding.Unicode.GetBytes(strJson);
                MemoryStream ms = new();
                CryptoStream cs = new(ms, desc.CreateEncryptor(byKey, byIV), CryptoStreamMode.Write);
                cs.Write(data, 0, data.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// 解密字符串
        /// </summary>
        /// <param name="datastr">需要解密的字符串</param>
        /// <param name="KEY_16">加密秘钥</param>
        /// <param name="IV_8">偏移量</param>
        /// <returns>解密后的字符串</returns>
        public static string GetCode(string datastr, string KEY_16, string IV_8)
        {
            byte[] byKey = Encoding.ASCII.GetBytes(KEY_16);
            byte[] byIV = Encoding.ASCII.GetBytes(IV_8);
            var desc = TripleDES.Create();
            var data = Convert.FromBase64String(datastr);
            MemoryStream ms = new();
            CryptoStream cs = new(ms, desc.CreateDecryptor(byKey, byIV), CryptoStreamMode.Write);
            cs.Write(data, 0, data.Length);
            cs.FlushFinalBlock();
            var TokenStr = Encoding.Unicode.GetString(ms.ToArray());
            return TokenStr;
        }
    }
}
