﻿using log4net;
using log4net.Config;
using log4net.Repository;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace EntityFrameworkCore.MigrationTool
{
    public class Log
    {
        private static ILoggerRepository _repository;
        private static ILog _log;

        private static ILog log
        {
            get
            {
                if (_log == null)
                {
                    Configure();
                }
                return _log;
            }
        }

        public static void Configure(string repositoryName = "NETCoreRepository", string configFile = "Config/log4net.config")
        {
            _repository = LogManager.CreateRepository(repositoryName);
            XmlConfigurator.Configure(_repository, new FileInfo(configFile));
            _log = LogManager.GetLogger(repositoryName, "");
        }

        public static void Info(string msg, string className = "")
        {
            log.Info(msg + ": " + className);
        }


        /// <summary>
        /// 描述：EFCore的执行日志
        /// 作者：杨钊
        /// 时间：2023-2-22
        /// </summary>
        /// <param name="_httpContextAccessor">ContextAccessor信息</param>
        /// <param name="message">日志信息</param>
        /// <returns></returns>
        public static void InfoEFCore(IHttpContextAccessor _httpContextAccessor, string message)
        {
            if (_httpContextAccessor != null)
            {
                var ip = _httpContextAccessor.HttpContext.Connection.LocalIpAddress.ToString();
                var id = _httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == "ID").Select(x => x.Value).FirstOrDefault();
                if (!id.IsNullOrWhiteSpace())
                {
                    log.Info("【InfoEFCore】【" + ip + "】【" + id + "】" + message);
                }
                else
                {
                    log.Info("【InfoEFCore】【" + ip + "】" + message);
                }
            }
            else
            {
                log.Info("【InfoEFCore】" + message);
            }
        }

        public static void Warn(string msg, string className = "")
        {
            log.Warn(msg + ": " + className);
        }

        public static void Error(string msg, string className = "")
        {
            log.Error(msg + ": " + className);
        }

        public static void Debug(string msg, string className = "")
        {
            log.Debug(msg + ": " + className);
        }

        /// <summary>
        /// 描述：过滤器Action日志
        /// 作者：杨钊
        /// 时间：2023-2-22
        /// <param name="ip">ip地址</param>
        /// <param name="id">用户id</param>
        /// <param name="dic">参数</param>
        /// <param name="actionName">方法名</param>
        /// <param name="controllerName">控制器名</param>
        public static void InfoOperation(string ip, long? id, IDictionary<string, object> dic, string actionName, string controllerName)
        {
            string param = string.Empty;
            string globalParam = string.Empty;
            foreach (var arg in dic)
            {
                string value = JsonConvert.SerializeObject(arg.Value);
                param += $"{arg.Key} : {value} \r\n";
                globalParam += value;
            }
            if (id != null)
            {
                Info("【" + id + "】【" + globalParam + "】", controllerName + "/" + actionName);
            }
            else
            {
                Info("【" + ip + "】【" + globalParam + "】", controllerName + "/" + actionName);
            }
        }
    }
}