﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using static System.Net.Mime.MediaTypeNames;

namespace EntityFrameworkCore.MigrationTool
{
    /// <summary>
    /// 字符串操作类
    /// </summary>
    public static class StringUtil
    {
        /// <summary>
        /// 清理换行，空格，\r
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RepString(this string str)
        {
            str = str.Replace("\r", "");
            str = str.Replace("\n", "");
            str = str.Replace(" ", "");
            return str;
        }
        /// <summary>
        /// 检测是否符合email格式
        /// </summary>
        /// <param name="strEmail">要判断的email字符串</param>
        /// <returns>判断结果</returns>
        public static bool IsValidEmail(this string strEmail)
        {
            return Regex.IsMatch(strEmail, @"^[\w\.]+@[A-Za-z0-9-_]+[\.][A-Za-z0-9-_]");
        }
        /// <summary>
        /// 检测是否是正确的Url
        /// </summary>
        /// <param name="strUrl">要验证的Url</param>
        /// <returns>判断结果</returns>
        public static bool IsURL(this string strUrl)
        {
            return Regex.IsMatch(strUrl, @"^(http|https)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{1,10}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&%\$#\=~_\-]+))*$");
        }
        /// <summary>
        /// 判断字符串是否为空或null或""或只有空格
        /// </summary>
        /// <param name="str">要验证的Url</param>
        /// <returns>判断结果</returns>
        public static bool IsNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }
        /// <summary>
        /// 分割字符串
        /// </summary>
        /// <param name="strContent">要分割的字符串</param>
        /// <param name="strSplit">分割符</param>
        /// <returns></returns>
        public static string[] RegexSplit(string strContent, string strSplit)
        {
            if (!strContent.IsNullOrWhiteSpace())
            {
                return Regex.Split(strContent, strSplit, RegexOptions.IgnoreCase);
            }
            else
            {
                return new string[0] { };
            }
        }
        /// <summary>
        /// 是否为ip
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool IsIP(this string ip)
        {
            return Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
        }
        /// <summary>
        /// 监测是否是数据类型
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static bool IsNumb(this PropertyInfo Model)
        {
            string Num = Model.PropertyType.Name;
            if (Num != "Double" && Num != "Int32" && Num != "Single" && Num != "Int16" && Num != "Int64" && Num != "Decimal")
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// Object转Json
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static string ToJson(this Object Model)
        {
            return JsonConvert.SerializeObject(Model);
        }
        /// <summary>
        ///Json转Object
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static T ToObject<T>(this string Model)
        {
            return JsonConvert.DeserializeObject<T>(Model);
        }
      
    }
}
