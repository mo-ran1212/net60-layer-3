﻿using Microsoft.Extensions.Configuration;
using System.Text;

namespace EntityFrameworkCore.MigrationTool
{
    public class AppHelper
    {
        private static IConfiguration _config;
        public static IConfiguration GetConfiguration() => _config;
        public AppHelper(IConfiguration configuration)
        {
            _config = configuration;
        }

        /// <summary>
        /// 读取指定节点的字符串
        /// </summary>
        /// <param name="sessions"></param>
        /// <returns></returns>
        public static string ReadAppSettings(params string[] sessions)
        {
            try
            {
                if (sessions.Any())
                {
                    return _config[string.Join(":", sessions)];
                }
            }
            catch
            {
                return "";
            }
            return "";
        }
        public static string GetJson(string jsonName)
        {
            var path = Directory.GetCurrentDirectory() + "/Config/" + jsonName;
            var json = "";
            using (FileStream fs = new FileStream(path, FileMode.Open, System.IO.FileAccess.Read, FileShare.ReadWrite))
            {
                using (StreamReader sr = new StreamReader(fs, Encoding.UTF8))
                {
                    json = sr.ReadToEnd().ToString();
                }
            }
            return json;
        }

    }
}
