﻿using EntityFrameworkCore.Supreme.SchemasModels;
using EntityFrameworkCore.Supreme;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkCore.MigrationTool;

namespace EntityFrameworkCore.Mysql.Extensions
{
    /// <summary>
    /// 描述：类的描述
    /// 作者：杨钊
    /// 时间：类的创建时间
    /// </summary>
    public static class DBUtils
    {
        /// <summary>
        /// 描述：获取雪花ID
        /// 作者：杨钊
        /// 时间：2023-2-28
        /// </summary>
        /// <returns></returns>
        public static long GetNewID()
        {
            IdWorker idWorker = new IdWorker(long.Parse(AppHelper.ReadAppSettings("IdWorker")));
            return idWorker.nextId();
        }
    }
}
