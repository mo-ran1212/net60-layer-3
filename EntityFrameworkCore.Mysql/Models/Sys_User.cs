using System;
using System.Collections.Generic;

namespace EntityFrameworkCore.Mysql.Models
{
    /// <summary>
    /// 用户表
    /// </summary>
    public partial class Sys_User : BaseEntity
    {
        /// <summary>
        /// 账号
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string UserPwd { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 是否启用（1：启用，0：禁用）
        /// </summary>
        public short IsEnable { get; set; }
       
    }
}