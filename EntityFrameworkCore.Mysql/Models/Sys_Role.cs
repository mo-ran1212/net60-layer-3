using System;
using System.Collections.Generic;

namespace EntityFrameworkCore.Mysql.Models
{
    /// <summary>
    /// 角色表
    /// </summary>
    public partial class Sys_Role : BaseEntity
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { get; set; }
        /// <summary>
        /// 角色描述
        /// </summary>
        public string RoleDesc { get; set; }
        /// <summary>
        /// 是否启用（1：启用，0：禁用）
        /// </summary>
        public int? IsEnable { get; set; }
    }
}