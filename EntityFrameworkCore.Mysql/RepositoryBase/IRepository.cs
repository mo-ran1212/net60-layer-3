﻿using EntityFrameworkCore.Mysql.Extensions;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkCore.Mysql.RepositoryBase
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {

        /// <summary>
        /// EF DBContext
        /// </summary>
        ZenithContext DbContext { get; }
        IQueryable<TEntity> AsNoTracking { get; }
        IQueryable<TEntity> AsTracking { get; }
        IQueryable<TEntity> AsQueryable { get; }
        IQueryable<TEntity> NoIsDelete { get; }

        /// <summary>
        /// 根据Id查询获得实体
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<TEntity> AsQueryableById(long key);

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        int Add(TEntity entity);


        /// <summary>
        /// 新增异步
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<int> AddAsync(TEntity entity);

        /// <summary>
        /// 新增批量
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        int AddRange(IEnumerable<TEntity> entity);

        /// <summary>
        /// 新增批量异步
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<int> AddRangeAsync(IEnumerable<TEntity> entity);

        /// <summary>
        /// 描述：执行事务
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <param name="action">action执行方法</param>
        /// <returns></returns>
        bool DbContextBeginTransaction(Action action);

        /// <summary>
        /// 描述：条件判断是否存在
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <param name="predicate">条件表达式</param>
        /// <returns></returns>
        Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// 描述：条件判断是否存在
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <param name="predicate">条件表达式</param>
        /// <returns></returns>
        bool Exists(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// 描述：修改
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        int Update(TEntity entity);

        /// <summary>
        /// 描述：批量修改
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        int UpdateRange(List<TEntity> entity);

        /// <summary>
        /// 描述：删除
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <param name="SuperviseDeptName">行政监管单位名称</param>
        /// <returns></returns>
        int Delete(TEntity entity, long id);

        /// <summary>
        /// 描述：根据主键删除
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <param name="key">主键key</param>
        /// <returns></returns>
        int DeleteById(long key, long id);

        /// <summary>
        /// 描述：根据主键批量删除
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <param name="key">主键keys</param>
        /// <returns></returns>
        int DeleteByIds(List<long> key, long id);

        /// <summary>
        /// 描述：批量删除
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <param name="SuperviseDeptName">行政监管单位名称</param>
        /// <returns></returns>
        int DeleteRange(IEnumerable<TEntity> entity,long id);

        /// <summary>
        /// 描述：保存
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <returns></returns>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// 描述：保存
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <returns></returns>
        int SaveChanges();
    }
}
