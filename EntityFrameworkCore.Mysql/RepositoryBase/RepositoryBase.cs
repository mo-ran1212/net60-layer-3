﻿using EntityFrameworkCore.Mysql.Extensions;
using Microsoft.EntityFrameworkCore.Storage;
using System.Linq.Expressions;

namespace EntityFrameworkCore.Mysql.RepositoryBase
{
    /// <summary>
    /// 仓库
    /// </summary>
    /// <typeparam name="TEntity">实体类</typeparam>
    public class RepositoryBase<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        public ZenithContext _entity;
        public RepositoryBase()
        {
        }
        /// <inheritdoc/>
        public RepositoryBase(ZenithContext entity)
        {
            _entity = entity;
        }
        /// <inheritdoc/>
        public virtual ZenithContext DbContext => _entity;
        /// <inheritdoc/>
        public virtual bool DbContextBeginTransaction(Action action)
        {
            using (IDbContextTransaction transaction = _entity.Database.BeginTransaction())
            {
                try
                {
                    action();
                    transaction.Commit();
                    return true;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    return false;
                }
            }
        }
        private DbSet<TEntity> DBSet => _entity.Set<TEntity>();
        /// <inheritdoc/>
        public IQueryable<TEntity> AsNoTracking => DBSet.AsNoTracking();
        /// <inheritdoc/>
        public IQueryable<TEntity> AsTracking => DBSet.AsTracking();
        /// <inheritdoc/>
        public IQueryable<TEntity> AsQueryable => DBSet.AsQueryable();
        /// <inheritdoc/>
        public IQueryable<TEntity> NoIsDelete => DBSet.AsQueryable();
        /// <inheritdoc/>
        public int SaveChanges()
        {
            return _entity.SaveChanges();
        }
        /// <inheritdoc/>
        public async Task<int> SaveChangesAsync()
        {
            return await _entity.SaveChangesAsync();
        }
        /// <inheritdoc/>
        public Task<TEntity> AsQueryableById(long key) => DBSet.AsTracking().Where(x => x.ID == key).FirstOrDefaultAsync();
        /// <inheritdoc/>
        public int Add(TEntity entity)
        {
            _entity.Add(entity);
            return SaveChanges();
        }
        /// <inheritdoc/>
        public async Task<int> AddAsync(TEntity entity)
        {
            await _entity.AddAsync(entity);
            return await _entity.SaveChangesAsync();
        }
        /// <inheritdoc/>
        public int AddRange(IEnumerable<TEntity> entity)
        {
            _entity.AddRange(entity);
            return SaveChanges();
        }
        /// <inheritdoc/>
        public async Task<int> AddRangeAsync(IEnumerable<TEntity> entity)
        {
            await _entity.AddRangeAsync(entity);
            return await _entity.SaveChangesAsync();
        }
        /// <inheritdoc/>
        public int Update(TEntity entity)
        {
            _entity.Update(entity);
            return SaveChanges();
        }
        /// <inheritdoc/>
        public int UpdateRange(List<TEntity> entity)
        {
            _entity.UpdateRange(entity);
            return SaveChanges();
        }
        /// <inheritdoc/>
        public int Delete(TEntity entity, long id)
        {
            _entity.Remove(entity);
            return SaveChanges();
        }
        /// <inheritdoc/>
        public int DeleteById(long key, long id)
        {
            var entity = DBSet.Where(x => key == x.ID).FirstOrDefault();
            return Delete(entity, id);

        }
        /// <inheritdoc/>
        public int DeleteByIds(List<long> key, long id)
        {
            var entitys = DBSet.Where(x => key.Contains(x.ID)).ToList();
            return DeleteRange(entitys, id);
        }
        /// <inheritdoc/>
        public int DeleteRange(IEnumerable<TEntity> entity, long id)
        {
            _entity.RemoveRange(entity);
            return SaveChanges();
        }
        /// <inheritdoc/>
        public virtual bool Exists(Expression<Func<TEntity, bool>> predicate) => DBSet.Any(predicate);
        /// <inheritdoc/>
        public virtual Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate) => DBSet.AnyAsync(predicate);

    }
}
