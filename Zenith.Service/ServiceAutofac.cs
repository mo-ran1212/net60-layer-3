﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Zenith.Service
{
    /// <summary>
    /// 描述：获取程序集名称
    /// 作者：杨钊
    /// 时间：2023-2-21
    /// </summary>
    public static class ServiceAutofac
    {
        /// <summary>
        /// 描述：获取程序集名称
        /// 作者：杨钊
        /// 时间：2023-2-21
        /// </summary>
        /// <returns></returns>
        public static string GetAssemblyName()
        {
            return Assembly.GetExecutingAssembly().GetName().Name;
        }
    }
}
